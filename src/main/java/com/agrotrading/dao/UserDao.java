package com.agrotrading.dao;

import com.agrotrading.models.User;


public interface UserDao {

	public boolean save(User user);
	public Integer findWithUsername(String email,String password);

}
