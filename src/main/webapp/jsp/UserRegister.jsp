<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="forms" uri="http://www.springframework.org/tags/form"%>
     <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
<style>
body{
	background: #383a3d;
	padding-top: 10vh;	
	height:500px;
}

form{
	background: #fff;
}
.form-container{
	border-radius: 10px;
	padding: 15px;
}

</style>
</head>
<body>
<div class="container">


<p style="text-align: center; color:#800000;">${mess}</p>
<section class="container-fluid">
<section class="row justify-content-center">
<section class="col-12 col-sm-4 col-md-6">
<forms:form  method="post" modelAttribute="user" class="form-container">
<fieldset class="form-group">
<div class="form-group">
          <h4 class="text-center font-weight-bold"> User Registration </h4>
<form:label path="firstname">FirstName</form:label>
<form:input path="firstname" type="text" required="required" name="firstname" class="form-control" ></form:input>

<form:errors path="firstname" cssClass="text-warning"></form:errors><br>
<form:label path="lastname">LastName</form:label>
<form:input path="lastname" type="text" required="required" name="lastname" class="form-control" ></form:input>
<form:errors path="lastname" class="text-danger"></form:errors><br>
<form:label path="contactnumber">ContactNumber</form:label>
<form:input path="contactnumber" type="text"  required="required" name="contactnumber" class="form-control" ></form:input>
<form:errors path="contactnumber" class="text-danger"></form:errors><br>
<form:label path="email">Email</form:label>

<form:input path="email" type="email" required="required" name="email" class="form-control" ></form:input>
<form:errors path="email" class="text-danger"></form:errors><br>
<form:label path="password">PassWord</form:label>
<form:input path="password" type="password" required="required" name="password" class="form-control" ></form:input>
<form:errors path="password" class="text-danger"></form:errors><br>
<form:label path="gender">Gender</form:label>
<form:select path="gender" name="gender" items="${GenderList}" class="form-control" /><br>
<form:label path="role">Role</form:label>
<form:select path="role" name="role" items="${Roles}" class="form-control"/><br>
<input type="submit" value="Register" class="btn btn-success"><br><br>
Already a User?<a href="login">Login</a>
</fieldset>
</forms:form>
</section>
    </section>
    </section>



</div>
</body>
</html>