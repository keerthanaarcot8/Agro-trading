package com.agrotrading.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.agrotrading.models.User;
@Component
public class UserDaoImpl implements UserDao {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Override
	public boolean save(User user) {
		int res=jdbcTemplate.update("insert into User values(?,?,?,?,?,?,?,?)",user.getUserid(),user.getFirstname(),user.getLastname(),user.getContactnumber(),user.getEmail(),user.getPassword(),user.getGender(),user.getRole());
		if(res>0) {
			return true;
		}
		return false;
	}

	@Override
	public Integer findWithUsername(String email, String password) {
PreparedStatementSetter setter = new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setString(1, email);
				ps.setString(2, password);
				
			}
		};
		
		return jdbcTemplate.query("select * from user where email=? and password=?", setter, new ResultSetExtractor<Integer>() {

			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
			
			
				if(rs.next())
			 {
					
					return rs.getInt(1);
				 
			 }
				return 0;
			}
		});
}
}
