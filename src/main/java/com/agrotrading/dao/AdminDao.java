package com.agrotrading.dao;

import com.agrotrading.models.Admin;

public interface AdminDao {

	public Integer findWithUsername(String username,String password);
}
