package com.agrotrading.dao;

import java.util.List;

import com.agrotrading.models.Pesticides;




public interface PesticideDao {
	int save(Pesticides p);
	int update(Pesticides p);
	int delete(int id);
	Pesticides getPesticideById(int id);
	List<Pesticides> getPesticides();
}
