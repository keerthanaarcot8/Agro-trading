package com.agrotrading.dao;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.agrotrading.models.Admin;

@Component
public class AdminDaoImpl implements AdminDao {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	

	@Override
	public Integer findWithUsername(String username, String password) {
PreparedStatementSetter setter = new PreparedStatementSetter() {
			
			@Override
			public void setValues(PreparedStatement ps) throws SQLException {
				ps.setString(1, username);
				ps.setString(2, password);
				
			}
		};
		
		return jdbcTemplate.query("select * from admin where username=? and password=?", setter, new ResultSetExtractor<Integer>() {

			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
			
			
				if(rs.next())
			 {
					
					return rs.getInt(1);
				 
			 }
				return 0;
			}
		});
	}

}
