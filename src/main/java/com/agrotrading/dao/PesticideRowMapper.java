package com.agrotrading.dao;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.agrotrading.models.Pesticides;




public class PesticideRowMapper implements RowMapper<Pesticides> {

	@Override
	public Pesticides mapRow(ResultSet rs, int rowNum) throws SQLException {
		Pesticides p=new Pesticides();
		p.setId(rs.getInt(1));
		p.setName(rs.getString(2));
		p.setPrice(rs.getDouble(3));
		p.setUnits(rs.getInt(4));
		return p;
		
	}

}
