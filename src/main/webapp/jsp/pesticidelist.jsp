<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<form:form method="post" action="save">
	<table align="center">
		<tr>
		<td><span>PesticideName:</span></td>
		<td><form:input type="text" path="name"/></td>
		</tr>
		<tr>
		<td><span>Price:</span></td>
		<td><form:input type="text" path="price"/></td>
		</tr>
		<tr>
		<td><span>Units:</span></td>
		<td><form:input type="text" path="units"/></td>
		</tr>
		<tr>
		<td></td>
		<td><input type="submit" value="Save"/>
		</tr>
	</table>

</form:form>
</body>
</html>