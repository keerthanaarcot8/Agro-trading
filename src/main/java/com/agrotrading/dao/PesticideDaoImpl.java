package com.agrotrading.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.agrotrading.models.Pesticides;



@Component
public class PesticideDaoImpl implements PesticideDao {

	@Autowired
	private JdbcTemplate template;
	
	@Override
	public int save(Pesticides p) {
		String sql="insert into pesticides(name,price,units) values('"+p.getName()+"','"+p.getPrice()+"',"+p.getUnits()+")";
		return template.update(sql);
	}

	@Override
	public int update(Pesticides p) {
String sql="update pesticides set name='"+p.getName()+"',price="+p.getPrice()+",units="+p.getUnits()+" where id="+p.getId()+"";
		
		
		return template.update(sql);
		
	}

	@Override
	public int delete(int id) {
		String sql="delete from student where id="+id+"";
		
		return template.update(sql);
	}

	@Override
	public List<Pesticides> getPesticides() {
		List<Pesticides> bookList=template.query("select * from pesticides",new PesticideRowMapper());
		return bookList;
	}

	@Override
	public Pesticides getPesticideById(int id) {
		String sql="select * from pesticides where id=?;";
		Pesticides b=template.queryForObject(sql,new PesticideRowMapper(),id);
		return b;
		
	}

}
