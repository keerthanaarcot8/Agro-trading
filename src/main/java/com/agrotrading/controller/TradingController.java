package com.agrotrading.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.agrotrading.dao.PesticideDao;
import com.agrotrading.models.Admin;
import com.agrotrading.models.Pesticides;
import com.agrotrading.models.User;
import com.agrotrading.services.AdminService;

import com.agrotrading.services.UserService;












@Controller
public class TradingController {
	@Autowired
	PesticideDao dao;
	@Autowired
	AdminService service;
	@Autowired
	UserService service1;
	
	
	@ModelAttribute("GenderList")
	public List<String> buildstate(){
		List<String> serviceMap=new ArrayList<String>();
		serviceMap.add("Select...");
		serviceMap.add("Male");
		serviceMap.add("Female");
		return serviceMap;
	}
	@ModelAttribute("Roles")
	public List<String> buildstate1(){
		List<String> serviceMap=new ArrayList<String>();
		serviceMap.add("Select...");
		serviceMap.add("Farmer");
		serviceMap.add("Wholesale Buyers");
		serviceMap.add("Experts");
		return serviceMap;
	}
	
	
	@RequestMapping(value="/",method=RequestMethod.GET)
	public String FirstPage() {
		return "index";
	}
	
	@RequestMapping(value="/adminpage",method = RequestMethod.GET)
	public String showAdminForm(Model m) {
		m.addAttribute("admin",new Admin());
		return "adminpage";
	}
	 @PostMapping(value="/adminpage")
	 public String login(@RequestParam(name="username")String username, @RequestParam(name="password") String password,HttpSession
	  session,Model model) {
		 int userid=service.checkAdmin(username, password);
		 if(userid>0) {
			
			 return "adminhome";
		 }
		 else {
			 model.addAttribute("mess","Entered Invalid username or password");
			 return "adminpage";
		 }
	 }
	 @RequestMapping(value="/login")
		public String showForm(Model m) {
			
			return "userpage";
		}
	
	 @RequestMapping(value="/register", method = RequestMethod.GET)
		public String addEmployee(Model model)
		{
			
			model.addAttribute("user",new User());
			return "UserRegister";
		}
	 @RequestMapping(value="/register",method=RequestMethod.POST)
	 public String addEmployee(@Valid @ModelAttribute ("user") User user,BindingResult result,HttpSession session1,Model model) {
		 if(result.hasErrors())
			{
				System.out.println("Error ");
				return "UserRegister";
			}
			
			service1.createNewUser(user);
			model.addAttribute("mess", "Your details are successfully submitted");
			return "UserRegister";
	 }
	 @PostMapping(value="/login")
	 public String userLogin(@RequestParam(name="email")String email, @RequestParam(name="password") String password,HttpSession
	  session,Model model) {
		 int userid=service1.checkUser(email, password);
		 if(userid>0) {
			
			 return "adminhome";
		 }
		 else {
			 model.addAttribute("mess","Entered Invalid email or password");
			 return "userpage";
		 }
	 }
	 @RequestMapping(value="/pesticidelist")
		public String showform(Model m) {
			m.addAttribute("command", new Pesticides());
			return "pesticidelist";
		}
	 @RequestMapping(value="/viewpesticidelist")
		public String viewstd(Model m) {
			List<Pesticides> list=dao.getPesticides();
			m.addAttribute("list",list);
			return "viewpesticidelist";
		}
	 
}
