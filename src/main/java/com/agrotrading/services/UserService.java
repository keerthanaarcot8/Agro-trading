package com.agrotrading.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.agrotrading.dao.UserDao;
import com.agrotrading.models.User;


@Service
public class UserService {

	@Autowired
	private UserDao dao;
	public void createNewUser(User user)
	{
		
		dao.save(user);
		
	}
public int checkUser(String email, String password) {
		
		int userid=dao.findWithUsername(email,password);
		if(userid>0)
		return userid;
		else {
			System.out.println("not found");
		return 0;
		}
	}

}
