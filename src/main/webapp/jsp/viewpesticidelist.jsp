<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Books List</h1>
<table align="center" border="2px">
<tr><th>Id</th><th>PesticideName</th><th>Price</th><th>Units</th><th>Update</th><th>Delete</th></tr>
<c:forEach var="b" items="${list}">
<tr>
<td>${b.id}</td>
<td>${b.name}</td>
<td>${b.price}</td>
<td>${b.units}</td>
<td><a href="editpestcide/${b.id }">Edit</a></td>
<td><a href="deletepesticide/${b.id }">Delete</a></td>
</tr>
</c:forEach>
</table>
</br>
<a href="pesticidelist">Add New Book</a>
</body>
</html>