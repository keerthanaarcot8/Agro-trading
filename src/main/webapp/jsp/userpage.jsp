<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
<style>
body{
	background: #383a3d;
	padding-top: 25vh;	
	height:500px;
}

form{
	background: #fff;
}

.form-container{
	border-radius: 10px;
	padding: 15px;
}


</style>
</head>
<body>
<p style="text-align: center; color:#800000;">${mess}</p>
<section class="container">
<section class="row justify-content-center">
<section class="col-12 col-sm-4 col-md-6">
<form class="form-container" method="post">
        <div class="form-group">
          <h4 class="text-center font-weight-bold"> User Login </h4>
          <label for="InputEmail1">Email Address</label>
           <input type="email" class="form-control"  placeholder="Enter email" name="email">
        </div>
        <div class="form-group">
          <label for="InputPassword1">Password</label>
          <input type="password" class="form-control" name="password" placeholder="Password">
        </div><br>
        <button type="submit" class="btn btn-primary btn-block">Submit</button>
        <div class="form-footer">
          <p> Don't have an account? <a href="register">Sign Up</a></p>
          
        </div>
        </form>
         </section>
    </section>
    </section>
</body>
</html>