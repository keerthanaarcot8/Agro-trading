package com.agrotrading.services;

import java.util.NoSuchElementException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.agrotrading.dao.AdminDao;
import com.agrotrading.models.Admin;

@Service
public class AdminService {

	@Autowired
	private AdminDao dao;
	
	
		
public int checkAdmin(String username, String password) {
		
		int userid=dao.findWithUsername(username,password);
		if(userid>0)
		return userid;
		else {
			System.out.println("not found");
		return 0;
		}
	}
}
